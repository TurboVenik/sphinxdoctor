import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;
import edu.cmu.sphinx.result.WordResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Main {



    public static void main(String[] args) throws IOException {
        Configuration configuration = new Configuration();
        configuration.setAcousticModelPath("src/main/resources/zero_ru_cont_8k_v3/zero_ru.cd_cont_4000");
        configuration.setDictionaryPath("src/main/resources/zero_ru_cont_8k_v3/ru.dic");
        configuration.setLanguageModelPath("src/main/resources/zero_ru_cont_8k_v3/ru.lm");
        configuration.setUseGrammar(false);
//        configuration.setSampleRate(8000);

        LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(configuration);

        recognizer.startRecognition(true);

        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {
//            for(WordResult word : result.getWords()) {
//                System.out.println(word);
//            }
            System.out.format("Hypothesis: %s\n", result.getHypothesis());

            System.out.println("List of recognized words and their times:");
            for (WordResult r : result.getWords()) {
                System.out.println(r);
            }

            System.out.println("Best 3 hypothesis:");
            for (String s : result.getNbest(3))
                System.out.println(s);

        }
        recognizer.stopRecognition();
    }
}
