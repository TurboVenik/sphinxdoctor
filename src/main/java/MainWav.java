import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class MainWav {

    public static void main(String[] args) throws Exception {

        Configuration configuration = new Configuration();

        configuration.setAcousticModelPath("src/main/resources/zero_ru_cont_8k_v3/zero_ru.cd_cont_4000");
        configuration.setDictionaryPath("src/main/resources/zero_ru_cont_8k_v3/ru.dic");
        configuration.setLanguageModelPath("src/main/resources/zero_ru_cont_8k_v3/ru.lm");

//        configuration.setSampleRate(8000);


        StreamSpeechRecognizer recognizer = new StreamSpeechRecognizer(configuration);


        InputStream stream = new FileInputStream(new File("src/main/resources/tt11.wav"));
        recognizer.startRecognition(stream);
        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {
            System.out.format("Hypothesis: %s\n", result.getHypothesis());
        }
        System.out.println(1);
        recognizer.stopRecognition();
        System.out.println(1);

        stream = new FileInputStream(new File("src/main/resources/zero_ru_cont_8k_v3/decoder-test.wav"));


        recognizer.startRecognition(stream);
        while ((result = recognizer.getResult()) != null) {
            System.out.format("Hypothesis: %s\n", result.getHypothesis());
        }
        System.out.println(1);
        recognizer.stopRecognition();
        System.out.println(1);
    }
}
