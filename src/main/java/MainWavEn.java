import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class MainWavEn {

    public static void main(String[] args) throws Exception {

        Configuration configuration = new Configuration();

        configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us");

        // You can also load model from folder
        // configuration.setAcousticModelPath("file:en-us");

        configuration.setDictionaryPath("resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict");
        configuration.setLanguageModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us.lm.bin");

//        configuration.setSampleRate(8000);


        StreamSpeechRecognizer recognizer = new StreamSpeechRecognizer(configuration);


        InputStream stream = new FileInputStream(new File("src/main/resources/tt7.wav"));
        recognizer.startRecognition(stream);
        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {
            System.out.format("Hypothesis: %s\n", result.getHypothesis());
        }
        System.out.println(1);
        recognizer.stopRecognition();
        System.out.println(1);

        stream = new FileInputStream(new File("src/main/resources/zero_ru_cont_8k_v3/decoder-test.wav"));


        recognizer.startRecognition(stream);
        while ((result = recognizer.getResult()) != null) {
            System.out.format("Hypothesis: %s\n", result.getHypothesis());
        }
        System.out.println(1);
        recognizer.stopRecognition();
        System.out.println(1);
    }
}
